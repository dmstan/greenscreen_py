#from imutils.video import VideoStream
import argparse
import datetime
#pythonimport imutils
import time
import cv2
import numpy as np
#import pyautogui
import tkinter as tk
import settings

def main():
    do_not_delete_frame = None #cv2.imread('backgrounds/evening_bg3.png')
    firstFrame = do_not_delete_frame

    # Input from WebCam 
    # DroidCam Video Source vs = cv2.VideoCapture('http://192.168.0.122:4747/mjpegfeed')
    # WebCam Video Source (built-in) vs = cv2.VideoCapture(0) 
    vs = settings.webcamsource()
    ret, frame = vs.read()

    # Create green screen image in the dimensions of the webcam frame
    green_image = np.zeros(frame.shape, np.uint8)
    green_image[:] = (0,177,0)
    dil_it = 55
    er_it = 50
    th_val = 55

    try:
        while vs.isOpened():
            ret, frame = vs.read()          

            # Transform frame to grayscale and blur to reduce noise in the absdiff frame
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            gray = cv2.GaussianBlur(gray, (21, 21), 0)

            # if the first frame is None, initialize it
            if firstFrame is None:
                firstFrame = gray
                continue    

            # compute the absolute difference between the current frame and
            # first frame
            frameDelta = cv2.absdiff(firstFrame, gray)

            # Apply a binary threshhold to the diff to get the areas you want to substitute
            #th3 = cv2.threshold(frameDelta, 10, 255, cv2.THRESH_BINARY)[1]   
            th3 = cv2.threshold(frameDelta,th_val,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)[1]

            # Broadens each displayed pixel. 
            # The higher the iterations, the broadening increases
            th3 = cv2.dilate(th3, None, iterations=dil_it)

            # Inverse of dilate
            th3 = cv2.erode(th3,None,iterations=er_it)


            # Create the inverse greenscreen mask for the bg
            # Apply the mask to the green screen image
            mask_inv = cv2.bitwise_not(th3)
            green_mask = cv2.bitwise_and(green_image,green_image, mask=mask_inv)

            # Apply the normal mask to the webcam frame
            # Add both frames for the final picture
            result = cv2.bitwise_and(frame,frame,mask = th3)
            result = cv2.add(green_mask,result)

            # show the frame and record if the user presses a key
            cv2.imshow("GreenScreen", result)

            key = cv2.waitKey(1) & 0xFF
            # if the `q` key was pressed, break from the loop
            if key == ord("q"):
                break
            # if the `f` key was pressed, take a new reference picture
            elif key == ord("f"):
                ret, frame = vs.read()
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                firstFrame = cv2.GaussianBlur(gray, (21, 21), 0)
            # save image to bg folder
            elif key == ord("s"):
                img_name = "backgrounds/bg_"+time.strftime('%d_%m_%H_%M')+".png"
                cv2.imwrite(img_name,firstFrame)
            elif key == ord("c"):
                dil_it, er_it, th_val, ff_name = settings.settings(dil_it,er_it,th_val)
                firstFrame = cv2.imread(ff_name)
                firstFrame = cv2.cvtColor(firstFrame, cv2.COLOR_BGR2GRAY)
                firstFrame = cv2.GaussianBlur(firstFrame, (21, 21), 0)

    finally:
        # Release video
        vs.release()
        cv2.destroyAllWindows()

