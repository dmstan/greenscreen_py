import cv2
import os
import re
import tkinter as tk

def settings(dil_it,er_it,th_val):
    root = tk.Tk()
    root.title("Settings") 
    
    tk.Label(root, text ='Dilate Iterations:', font = "50").pack()
    dilate_it = tk.IntVar()
    dilate_it.set(dil_it)
    tk.Scale(root, from_ = 0, to = 100, variable=dilate_it, orient = tk.HORIZONTAL).pack()
    
    tk.Label(root, text ='Erode Iterations:', font = "50").pack()
    erode_it = tk.IntVar()
    erode_it.set(er_it)
    tk.Scale(root, from_ = 0, to = 100, variable=erode_it, orient = tk.HORIZONTAL).pack()
        
    tk.Label(root, text ='Threshhold value:', font = "50").pack()
    thresh_val = tk.IntVar()
    thresh_val.set(th_val)
    tk.Scale(root, from_ = 0, to = 100, variable=thresh_val, orient = tk.HORIZONTAL).pack() 
    
    #file_name = tk.StringVar()
    #tk.Entry(root, text = 'Enter bg filename', textvariable=file_name)
    #tk.Button(root, text = 'Save firstFrame', command = saveFirstFrame(file_name.get())).pack(pady = 10)
    
    
    files = os.listdir('backgrounds')
    files = filter(lambda file: re.match("^.*[a-z,0-9]\.png",file),files)
   
    # datatype of menu text 
    clicked = tk.StringVar() 
  
    # Create Dropdown menu 
    drop = tk.OptionMenu( root , clicked , *files ) 
    drop.pack() 
    
    tk.Button(root, text = 'OK', command = root.destroy).pack(pady = 10)
    root.mainloop() 

    return dilate_it.get(), erode_it.get(), thresh_val.get(), 'backgrounds/'+clicked.get()

def saveFirstFrame(file_name):
    cv2.imwrite(file_name, firstFrame)
    
def webcamsource():
    root = tk.Tk() 
    root.title("VideoSource") 
    label = tk.Label(root, text ="Choose video source").pack()
    v = tk.IntVar() 
    tk.Radiobutton(root, text='Default', variable=v, value=1).pack(anchor=tk.W) 
    tk.Radiobutton(root, text='DroidCam', variable=v, value=2).pack(anchor=tk.W) 
    tk.Button(root, text = 'OK', command = root.destroy).pack(pady = 10)
    root.mainloop() 

    if v.get() == 1:
        return cv2.VideoCapture(0)
    elif v.get() == 2:
        return cv2.VideoCapture('http://192.168.0.122:4747/mjpegfeed')